var now=new Date()
var thisyear=now.getFullYear()//2020 tahun sekarang
arr=[]
function arrayToObject(arr){
    for(i=0;i<arr.length;i++){
        var personObj={
            firstName: arr[i][0],
            lastName : arr[i][1],
            gender:arr[i][2],
            age:thisyear - arr[i][3]
        }

        console.log(i+1 + ". "+personObj.firstName+" "+personObj.lastName+":");
        console.log(personObj)
    }
}

// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""