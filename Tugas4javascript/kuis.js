function balikkata(kata){
    let balik="";
    for (let i=kata.length-1;i>=0;i--){
        balik = balik + kata[i];
    }
    return balik;
}

console.log(balikkata("Kasur Rusak"));

function generateBoard(){
  var board = [];
  var tempRow = [];
  var rowEven = true;
  
  for (var row = 10; row >= 1; row--){
    for (let i = 1; i <= 10; i++){
      rowEven ?
        tempRow.push( 10 * ( row - 1 ) + ( 11 - i )) : 
        tempRow.push( 10 * ( row - 1 ) + i );
      }
    rowEven = !rowEven;
    board.push(tempRow);
    tempRow = [];
  }
  
  return board;
}

console.log(generateBoard());