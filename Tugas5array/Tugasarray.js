let log=console.log;
log("Nomor 1");
function range(a,b){
    let array=[]
    if (a<b){
        for (let i=a; i<=b;i++){
            array.push(i);
        }
    }
    else if(a>b){
        for (let i=a;i>=b;i--){
            array.push(i);
        }
    }
    else {
        array.push(-1);
    }
    return array;
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

log("\nNomor 2")
function rangewithstep(angka1,angka2,step){
    let array1=[]
    if (angka1<angka2){
        for(let y=angka1;y<=angka2;y+=step){
            array1.push(y);
        }
    }
    else if(angka1>angka2){
        for (let y=angka1;y>=angka2;y-=step){
            array1.push(y);
        }
    }
    return array1;
}
console.log(rangewithstep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangewithstep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangewithstep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangewithstep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
log("\nNomor 3");
function sum(awal,akhir,step=1){
    let nilai=[];
    let total=0;
    if (awal<akhir){
    for (i=awal;i <= akhir;i+=step){
        nilai.push(i);
        total=total+i;
    }
}
else if(awal>akhir){
    for(i=awal;i>=akhir;i-=step){
        nilai.push(i);
        total=total+i;
    }
}
else{
    nilai.push(1)
}
return total
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

log("\nNomor 4");
var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ] 
function datahandling(input){
    for(var i=0;i<input.length;i++){
        for(var j=0;j<input[i].length;j++){
        var value=input[i][j]
        var text = "";
            switch(j) {
                case 0:
                    text="nomor ID: " + value
                    break
                case 1:
                    text = "Nama lengkap: " + value
                    break
                case 2:
                    var value2=input[i][3]
                    text = "TTL: " + value + " " + value2
                    break
                case 3:
                break
                case 4:
                    text = "Hobi: " + value + "\n"
                    break

            }
            if(j !== 3){
                log(text)
            }
        }
    }
}
datahandling(input)


log("\nNomor5");
function balikkata(kata){
    let balik="";
    for (let i=kata.length-1;i>=0;i--){
        balik = balik + kata[i];
    }
    return balik;
}
console.log(balikkata("Kasur Rusak"));
console.log(balikkata("SanberCode")) // edoCrebnaS
console.log(balikkata("Haji Ijah")) // hajI ijaH
console.log(balikkata("racecar")) // racecar
console.log(balikkata("I am Sanbers")) // srebnaS ma I 