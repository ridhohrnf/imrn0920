
console.log("\nlooping pertama\n");

var flag = 1;
var dua = 2;
while(flag < 11) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
  
  console.log(flag * dua + " - I love coding"); // Menampilkan nilai flag pada iterasi tertentu
  flag++; // Mengubah nilai flag dengan menambahkan 1
}

console.log("\nlooping kedua \n");
var deret = 20;

while(deret >= 1 ) { 
  console.log(deret + " - I will become mobile developer");
  deret -= 2;
}
console.log("\nlooping for\n");
for (var ganjil =1; ganjil <=20; ganjil ++){
  if(ganjil % 2 ==1 ){
    console.log(ganjil +" - santai");
  }
  if (ganjil % 2 == 0){
    console.log(ganjil +" - berkualitas")
  }
  if(ganjil % 3 == 0 && ganjil % 2 == 1){
    console.log(ganjil +" - I love coding")
  }
}
console.log("\nlooping persegi panjang\n")
for(var angka = 1; angka < 5; angka++) {
  console.log('########');
} 
console.log("\nLooping segitiga");
for (var x=1;x<=6;x++){
  console.log(" ");
  for (var y = 1;y <=x;y++){
    process.stdout.write("#")
  }
}
console.log("\n\nLooping papan catur\n");
for (chessboard=1;chessboard<=8;chessboard++){
  if(chessboard %2==1){
    console.log(" # # # #");
  }
  else console.log("# # # #");
}
