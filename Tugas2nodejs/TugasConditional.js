console.log("Soal if else");
var nama = "Ridho";
var peran = "";
var enter = " ";
if (nama != null) {
    console.log("Halo "+nama+ ", pilih peranmu untuk memulai game");
}
else {
    console.log("Nama anda harus diisi!")
}
console.log(enter);
var nama = "Ridho";
var peran = "penyihir";
if (peran == "penyihir"){
    console.log("Selamat datang di dunia Werewolf, "+nama);
    console.log("Halo Penyihir"+nama+" kamu dapat melihat siapa yang menjadi werewolf!");
} else if(peran == "guard"){
    console.log("Selamat datang di dunia Werewolf, " +nama);
    console.log("Halo Guard "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.");
} else if(peran == "werewolf"){
    console.log("Selamat datang di dunia Werewolf, "+nama);
    console.log("Halo Werewolf "+nama+", Kamu akan memakan mangsa setiap malam!")
}
console.log(enter);
var nama="fanny";
var peran="guard";
if (peran == "penyihir"){
    console.log("Selamat datang di dunia Werewolf, "+nama);
    console.log("Halo Penyihir"+nama+" kamu dapat melihat siapa yang menjadi werewolf!");
} else if(peran == "guard"){
    console.log("Selamat datang di dunia Werewolf, " +nama);
    console.log("Halo Guard "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.");
} else if(peran == "werewolf"){
    console.log("Selamat datang di dunia Werewolf, "+nama);
    console.log("Halo Werewolf "+nama+", Kamu akan memakan mangsa setiap malam!")
}
console.log(enter);
var nama="rifa";
var peran="werewolf";
if (peran == "penyihir"){
    console.log("Selamat datang di dunia Werewolf, "+nama);
    console.log("Halo Penyihir"+nama+" kamu dapat melihat siapa yang menjadi werewolf!");
} else if(peran == "guard"){
    console.log("Selamat datang di dunia Werewolf, " +nama);
    console.log("Halo Guard "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.");
} else if(peran == "werewolf"){
    console.log("Selamat datang di dunia Werewolf, "+nama);
    console.log("Halo Werewolf "+nama+", Kamu akan memakan mangsa setiap malam!")
}
console.log(enter);
console.log("Soal Switch and case");
var name = "fanny";
var tanggal = 11; 
var bulan = 4;
var tahun = 1999; 
switch(bulan) {
  case 1: { console.log(name +" lahir pada "+ tanggal + ' Januari ' + tahun); break; }
  case 2: { console.log(name +" lahir pada "+ tanggal + ' Februari ' + tahun); break; }
  case 3: { console.log(name +" lahir pada "+ tanggal + ' Maret ' + tahun); break; }
  case 4: { console.log(name +" lahir pada "+ tanggal + ' April ' + tahun); break; }
  case 5: { console.log(name +" lahir pada "+ tanggal + ' Mei ' + tahun); break; }
  case 6: { console.log(name +" lahir pada "+ tanggal + ' Juni ' + tahun); break; }  
  case 7: { console.log(name +" lahir pada "+ tanggal + ' Juli ' + tahun); break; }
  case 8: { console.log(name +" lahir pada "+ tanggal + ' Agustus ' + tahun); break; }
  case 9: { console.log(name +" lahir pada "+ tanggal + ' September ' + tahun); break; }
  case 10: { console.log(name+ " lahir pada " + tanggal + ' Oktober ' + tahun); break; }
  case 11: { console.log(name+ " lahir pada " + tanggal + ' November ' + tahun); break; }
  case 12: { console.log(name+ " lahir pada " + tanggal + ' Desember ' + tahun); break; }
  default : { console.log('Bulan tidak tersedia');} }